# readlink /proc/$$/exe

# This code sets all of the Env Vars required for the code to run. 
# If being ran directly run as such `source ./bin/set_paths.sh

# GLOBAL COLORS for other scripts;
export SA_RED='\033[0;31m';
export SA_GREEN='\033[0;32m';
export SA_BLUE='\033[0;36m';
export SA_YELLOW='\033[1;33m';
export SA_NC='\033[0m';

# Logic to avoid confussion if this file is not sources
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then 
    echo -e "script ${BASH_SOURCE[0]}: 
is not being sourced if you are running this directly plsease use: 
${SA_RED}\`source ./sbin/set_paths.sh\`
Enviorment Vars Not Set"${SA_NC}
    exit 1;
fi

# Set the aws params so all applications can use them if this file is sources
export AWS_ACCESS_KEY_ID="AKIASE2VZGI2WAYSL6N5"
export AWS_SECRET_ACCESS_KEY="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"
export AWS_DEFAULT_REGION="us-east-1"

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

#Sets all the global env stuff needed for the apps to run;
export APP_ROOT=$SCRIPT_DIR/../;
export GOPATH=$APP_ROOT/go_path;
export LOG_DIR=$APP_ROOT/log;
export CONFIG_DIR=$APP_ROOT/etc;

# Makes go tools available on the path;
export PATH=$GOPATH/bin:$PATH;

# This will be done after the venv is sourced (THIS is going to go in the application(s) run script)
# export APPEND_TO_PYTHONPATH=$APP_ROOT/screenshot_engine:$APP_ROOT/img_archiver_engine;

# TODO: add error detection and exit
echo -e "${SA_GREEN}Enviorment Vars Successfully Set${SA_NC}";