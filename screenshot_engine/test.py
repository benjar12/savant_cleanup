from cython.parallel import parallel, prange
from cython import nogil, gil

import time

# # pip install git+https://github.com/ZoomerAnalytics/cimport
# from libc.stdlib cimport abort, malloc, free
# from libc.stio cimport
# from cpython import cimport

result = []

with parallel(4):
    cnt: int = 0
    while cnt > 20: 
        print("foo thread: {0}, cnt: {1}".format(parallel.threadid(), cnt))


time.sleep(10)