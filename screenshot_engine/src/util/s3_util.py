import boto3
import uuid
import os

from datetime import datetime

from src.util import config
from src.util import execute_screenshot

'''
    write_bytes_to_s3 is pretty straight forward pass in bytes, filename, extention
'''

total_failed_s3img = 0

import logging
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('nose').setLevel(logging.CRITICAL)

def check_arg_exists(dict_args, arg):
    global total_failed_s3img
    if arg not in dict_args:
        total_failed_s3img += 1
        config.log_msg("Missing required argument file write will likely fail. Number of failures today: {0}".format(total_failed_s3img), name="S3", level="warning")
    return dict_args[arg]

def write_bytes_to_s3(**kwargs):
    logging.getLogger('boto3').setLevel(logging.CRITICAL)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('nose').setLevel(logging.CRITICAL)
    body = check_arg_exists(kwargs, "body")
    filename = check_arg_exists(kwargs, "filename")
    if "ext" in kwargs:
        ext = kwargs["ext"]
    else:
        ext = "png"

    bucket = config.get_config_value("aws.image_archive_bucket")
    tldir = config.get_config_value("aws.path_prefix")
    key = "{0}/{1}.{2}".format(tldir, filename, ext)

    client = boto3.client('s3')
    client.put_object(Body=body, Bucket=bucket, Key=key)

if __name__ == "__main__":
    body = execute_screenshot.capture_screen()
    date = datetime.now().strftime('%Y/%m/%d')
    write_bytes_to_s3(body=body, filename="localdev/{0}/{1}".format(date, str(uuid.uuid1())))