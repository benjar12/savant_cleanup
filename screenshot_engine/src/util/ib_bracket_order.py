from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import ibConnection, message

from src.util import config

def check_arg_exists(dict_args, arg):
    if arg not in dict_args:
        config.log_msg("Missing required argument {0}. trade will fail.".format(arg), name="IB", level="err")
    return dict_args[arg]

def bracket_order(**kwargs):

    # Get order params
    order_id:int = check_arg_exists(kwargs, "order_id")
    quantity:float = check_arg_exists(kwargs, "quantity")
    limit_price:float = check_arg_exists(kwargs, "limit_price")
    take_profit_limit_price:float = check_arg_exists(kwargs, "take_profit_limit_price")
    stop_loss_price:float = check_arg_exists(kwargs, "stop_lose_price")
    action = "BUY"

    # Our buy order
    parent = Order()
    parent.orderId = order_id
    parent.action = action
    parent.orderType = "LMT"
    parent.totalQuantity = quantity
    parent.lmtPrice = limit_price
    parent.transmit = False

    # Our profit order
    take_profit = Order()
    take_profit.orderId = parent.orderId + 1
    take_profit.action = "SELL" if action == "BUY" else "BUY"
    take_profit.orderType = "LMT"
    take_profit.totalQuantity = quantity
    take_profit.lmtPrice = take_profit_limit_price
    take_profit.parentId = order_id
    take_profit.transmit = False

    # Our cut loss order
    stop_loss = Order()
    stop_loss.orderId = parent.orderId + 2
    stop_loss.action = "SELL" if action == "BUY" else "BUY"
    stop_loss.orderType = "STP"
    stop_loss.auxPrice = stop_loss_price
    stop_loss.totalQuantity = quantity
    stop_loss.parentId = order_id
    stop_loss.transmit = True

    # Return a list of orders to run. These orders have to be ran in that order
    # or shit hits the fan. 
    return [parent, take_profit, stop_loss]