import gevent

from gevent.threadpool import ThreadPool

from src.util import config

'''

'''
class AsyncTaskManager(object):
    def __init__(self):
        img_writers = config.get_config_value("concurrency.s3_writers")

        # INIT the pools
        self.s3_pool = ThreadPool(img_writers)

    def s3_image_writter_pool(self, Method, **kwargs):
        self.s3_pool.spawn(Method, **kwargs)

    def historical_writer_pool(self, Method, **kwargs):
        config.log_msg("History Writer Not Implemented!", level="warn")


    def run_function_with_list_of_data_async(self, Method, DataList):
        # Create list of jobs
        jobs = [gevent.spawn(Method, data) for data in DataList]

        # Wait for done
        gevent.joinall(jobs)

        # Gather and return results
        # TODO: check for errors
        return [job.get() for job in jobs]

    