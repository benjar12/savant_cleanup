import os
import uuid
import time
import mss

# from mss import tools
# from PIL import ImageGrab
import pyscreenshot as sc

from src.util import config
from src.util import rand_utils

'''
    NOTE
    PLEASE READ. On osx for this to work terminal needs permissions to record the screen.
    You will need to go to System Prefrences > Security > Accesibility > Screan Capture > Check terminal.
    This code will give you a blank background otherwise.
    NOTE
    TO TEST YOU CAN RUN:
    ./bin/run_other_file_in_screen_capture.sh {YOUR_VENV} screenshot_engine/src/util/execute_screenshot.py 
    This will write the data to your /tmp folder
    TO TEST IT GOING TO S3 RUN:
    ./bin/run_other_file_in_screen_capture.sh {YOUR_VENV} screenshot_engine/src/util/s3_util.py 
'''

'''
    def capture_screen grabs the screenshot and converts to bytes
    The bytes will be used in both send to textact and the s3 util to 
    archive the raw image. This will be done in a async manner.
'''
def capture_screen():
    l = config.get_config_value("capture.left_offset")
    t = config.get_config_value("capture.top_offset")
    w = config.get_config_value("capture.width")
    h = config.get_config_value("capture.hieght")
    bbox=(l,t,w,h)
    
    with mss.mss() as sct:
        # Grab the picture
        im = sct.grab(bbox)
        # Return the raw bytes
        return mss.tools.to_png(im.rgb, im.size)

'''
    Just some dummy code to see it does something
    eventually tdd will be incorperated
'''
if __name__ == "__main__":
    png_bytes = capture_screen()
    rand_postfix = rand_utils.random_string_generator(size=4)
    abs_ppath = "/tmp/test_screenshot_savant_{0}.png".format(rand_postfix)

    with open(abs_ppath, "wb") as o:
        o.write(png_bytes)

    config.log_msg("file written. goodbye")