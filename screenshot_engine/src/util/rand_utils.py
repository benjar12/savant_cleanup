import random
import string

def random_string_generator(size=10, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


# This is kinda redundant, but for client i'd with ib we have a specific range
def rand_int(start=400, end=899):
    return random.randint(start, end)