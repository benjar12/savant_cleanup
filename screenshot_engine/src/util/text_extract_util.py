import boto3
import os
import csv
import json
import io
import re
import pprint
import difflib

from io import StringIO
from datetime import datetime

from src.util import config

'''
    This file is pretty simple sends the bytes to AWS Text Extract and returns.
    These files that talk to external services should really be refactorsed into the
    service package.

    PARAM: @STRING object_key this is where is should be in s3. I want this because if we fail.
    it would be nice to see why
    PARAM: @BYTE_ARRAY contains the png in a byte form

    NOTE:
        I'm sorry if anyone ever has to tuch this code it's all of coping drunken
        code. Honestly i'm Suprised it works. Ben Jarman<benjarman@me.com>
'''
# Next Time to setup some helper functions
def get_rows_columns_map(table_result, blocks_map):
    rows = {}
    for relationship in table_result['Relationships']:
        if relationship['Type'] == 'CHILD':
            for child_id in relationship['Ids']:
                cell = blocks_map[child_id]
                if cell['BlockType'] == 'CELL':
                    row_index = cell['RowIndex']
                    col_index = cell['ColumnIndex']
                    if row_index not in rows:
                        # create new row
                        rows[row_index] = {}

                    # get the text value
                    rows[row_index][col_index] = get_text(cell, blocks_map)
    return rows


def get_text(result, blocks_map):
    text = ''
    if 'Relationships' in result:
        for relationship in result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    word = blocks_map[child_id]
                    if word['BlockType'] == 'WORD':
                        text += word['Text'] + ' '
                    if word['BlockType'] == 'SELECTION_ELEMENT':
                        if word['SelectionStatus'] == 'SELECTED':
                            text += 'X '
    return text


@DeprecationWarning
def get_table_csv_results_from_file(file_name):
    with open(file_name, 'rb') as file:
        img_test = file.read()
        bytes_test = bytearray(img_test)
        print('Image loaded', file_name)

    # process using image bytes
    # get the results
    client = boto3.client('textract')

    response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

    # Get the text blocks
    blocks = response['Blocks']
    # can be used for debugging reasons
    # pprint(blocks)

    blocks_map = {}
    table_blocks = []
    for block in blocks:
        blocks_map[block['Id']] = block
        if block['BlockType'] == "TABLE":
            table_blocks.append(block)

    if len(table_blocks) <= 0:
        return "<b> NO Table FOUND </b>"

    csv = ''
    for index, table in enumerate(table_blocks):
        csv += generate_table_csv(table, blocks_map, index + 1)
        csv += '\n\n'

    return csv

# TODO: break up this function a bit
# TODO: i want to break this into the csv processing file and the amazon textract service.
def get_table_csv_results(object_key, bytes_body):
    # Not sure if its bad to create a connection per request but im pretty positive
    # Amazon would be doing connection pooling
    client = boto3.client('textract')

    response = client.analyze_document(Document={'Bytes': bytes_body}, FeatureTypes=['TABLES'])

    # Get the text blocks
    blocks = response['Blocks']

    blocks_map = {}
    table_blocks = []
    for block in blocks:
        blocks_map[block['Id']] = block
        if block['BlockType'] == "TABLE":
            table_blocks.append(block)

    # TODO: Ifeel like we should be doing more here if we can't parse under
    # stable conditions we have an issue;
    if len(table_blocks) <= 0:
        config.log_msg("Failed to find any tables in {0}".format(object_key), level="warn")
        return

    # TODO: Why are we converting this to a csv when in the end its goona be a
    # Dict or a list of dict's. I dug this hole so at some point we need to rewrite
    # all of this code. I expect O(log(n)) complexity max.
    csv = ''
    for index, table in enumerate(table_blocks):
        csv += generate_table_csv(table, blocks_map, index + 1)
        csv += '\n\n'

    return csv


def clean_number_strings_convert_to_float(s):
    return float(re.sub('[^0-9]','', s))


# Helper function to convert millions to a numarical value
def replace_m_with_correct_number(input_string):
    try:
        if input_string.endswith("M") or input_string.endswith("N"):
            return str(clean_number_strings_convert_to_float(input_string) * 1000000)
        elif input_string.endswith("B"):
            return str(clean_number_strings_convert_to_float(input_string) * 10000000)
        elif input_string.endswith("K"):
            try:
                return str(clean_number_strings_convert_to_float(input_string) * 1000)
            except Exception:
                return input_string
        else:
            return input_string
    except Exception:
        return input_string


def generate_table_csv(table_result, blocks_map, table_index):
    rows = get_rows_columns_map(table_result, blocks_map)

    table_id = 'Table_' + str(table_index)

    # get cells.
    # csv = 'Table: {0}\n\n'.format(table_id)
    csv = ""

    for row_index, cols in rows.items():

        for col_index, text in cols.items():
            # TODO Break this up and use a csv encoder. gonna eventually fuck shit up
            csv += '{}'.format(replace_m_with_correct_number(text.rstrip().strip("%"))) + ","
        csv += '\n'

    csv += '\n\n\n'
    return csv


# This is a shit workaround to get rid of the top bar info thats being processed as a table
def contains_top_bar_remove(input_table_str):
    if "SHORT" in input_table_str or "STOCK DATA" in input_table_str:
        print("in the fuct")
        return "\n".join(input_table_str.splitlines()[1:])
    else:
        return input_table_str


# TODO: make this shit work
# Helper func that checks similarity of strings
def fix_header(input, expected):
    output = input
    seq = difflib.SequenceMatcher(None, input, expected)
    if seq.ratio() * 100 > 75:
        output = expected
    return output


# This needs to be fixed very ugly way of converting strings to floats
# TODO: Break this function up. its doing to much
def convert_to_dist(str):

    is_first_row = True
    is_first_signal_key = True

    f = StringIO(str)
    reader = csv.reader(f, delimiter=',')
    headers = []
    data = []
    for row in reader:
        current_row = {}

        if is_first_row == True:

            for column in row:
                # Right now were only fixing Signa to be Signal there will be more corrections i'm sure.
                c = fix_header(column, "Signal")
                c = fix_header(c, "Symbol")

                # Lets next get rid of anything but letters
                c = re.sub(r'[^A-Za-z]', '', c)

                # Now we need to handle the double signal key issue
                if c == "Signal" and is_first_signal_key == True:
                    c = "SignalN"
                    is_first_signal_key = False
                elif c == "Signal":
                    c = "SignalP"

                headers.append(c)

            is_first_row = False

        else:

            if len(row) == 0:
                continue

            for index, column in enumerate(row):
                if column == "":
                    continue
                # Next lets convert it to a number if possiable
                try:
                    new_item = float(column)
                except Exception:
                    new_item = column

                h = headers[index]
                current_row[h] = new_item
            data.append(current_row)

    return data

def run_all_proccessing(object_key, inputbytes):
    # set some vars
    captured_at = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')

    # Process image to list of dict
    csv_data = get_table_csv_results(object_key, inputbytes)
    dict_data = convert_to_dist(csv_data)

    # Add aditional data and return
    return [dict(elm, capturedAt=captured_at, ObjectKey=object_key) for elm in dict_data]
