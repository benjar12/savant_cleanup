import os
import json
import logging
import sys

from dotty_dict import dotty

# TODO: globals bad fix this shit
__screenshot_dotty_config=dotty()
__screenshot_conf_is_loaded=False

def __init_config_if_not_already():
    global __screenshot_dotty_config
    global __screenshot_conf_is_loaded
    # If already loaded return
    if __screenshot_conf_is_loaded:
        return

    # Get the path to config dir if set else default to trading_config.json
    # I put this here because if the source is compiled i don't want braxton
    # to have to use a terminal
    # TODO: make this consistent. If the dir is set the file name must be
    # screenshot_engine.json inside of the dir. otherwise it goes to a default
    # file of trading.json This is slutty code. I just don't fully know the impact
    # of changing it.
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
    elif "CONFIG_DIR" in os.environ:
        config_dir = os.environ["CONFIG_DIR"]
        config_path = config_dir + "/screenshot_engine.json"
    else:
        config_path = os.path.dirname(sys.executable) + "/trading_config.json"
    
    # Test The File
    if not os.path.isfile(config_path):
        log_msg("Could not load connfig file!", level="err", cant_recover=True)

    # Read the file
    with open(config_path) as f:
        __screenshot_dotty_config=dotty(json.load(f))
        __screenshot_conf_is_loaded=True

    # I'm doing this because I don't want braxton to have to set env vars.
    # It's not clean at all! Could end up overflowing the stack with one small bug!
    os.environ["AWS_ACCESS_KEY_ID"] = get_config_value("aws.AWS_ACCESS_KEY_ID")
    os.environ["AWS_SECRET_ACCESS_KEY"] = get_config_value("aws.AWS_SECRET_ACCESS_KEY")
    os.environ["AWS_DEFAULT_REGION"] = get_config_value("aws.AWS_DEFAULT_REGION")
        

'''def get param lazy loads the configuration from disk
    @ARG param_path: this is dot notation for exploring python representations
    of the json file. EXAMPLE: capture.left_offset would return 267 
'''

# TODO: add error handling
def get_config_value(dot_path):
    __init_config_if_not_already()
    return __screenshot_dotty_config.get(dot_path)

# Yeah i know this is a useless function call
# Logging should be it's own thing, but until it gets more complete .....
# Logging and configurations are two of the most common circular imports issues
# TODO: deslutify this code. I should have made this a class
__loggers__ = {}
def __init_logger(logger_name):
    logger_name = logger_name.upper()

    if logger_name not in __loggers__:
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.INFO)

        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.INFO)

        logger.addHandler(consoleHandler)

        formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s: %(message)s')
        consoleHandler.setFormatter(formatter)
        __loggers__[logger_name] = logger

    return __loggers__[logger_name]

# TODO: more than just standard out
# On the ops side i want log rotations in place first.
def log_msg(msg, name="ROOT", level="info", cant_recover=False):
    active = __init_logger(name)
    if level  == "warn" or level == "warning":
        active.warning("\033[1;33m{0}\033[0m".format(msg))
    elif level == "err" or level == "error":
        active.error("\033[1;31m{0}\033[0m".format(msg))
    elif level == "success":
        active.info("\033[1;32m{0}\033[0m".format("the application ran without errors. Cya tomorrow!"))
    else:
        active.info("\033[1;36m{0}\033[0m".format(msg))

    if cant_recover:
        active.error("\033[1;31mI the program have hit a case where I can not recover. It is my job to die and donate my logs to science;\033[0m")
        sys.exit(1)

# TODO: this main function is hard coded to my absolute path not very helpful for others debugging
if __name__ == "__main__":
    os.environ['CONFIG_DIR'] = '/Volumes/Data/Development/savant_cleanup/etc/'
    print(get_config_value('outbound_socket.host'))