from src.util import config
import json

'''
    NOTE These are python dictionaries not json strings

    INPUT DATA:
    {
        "Max":4.86,
        "Float":203000000.0,
        "SR":0.16,
        "MkCap":94000000.0,
        "Symbol":"ADXS",
        "Last":0.382,
        "CapturedAt":"2019-10-09T05:00:56.100481",
        "Volume":106000000.0,
        "SignalP":0.37,
        "Resistance":0.39,
        "LocalFileLocation":"/tmp/20191009/12108a3a-ea84-11e9-99f5-dca9046e9d71.png",
        "Min":-2.3,
        "Support":0.31,
        "LongDelta":0.02,
        "ShortDelta":0.02,
        "SignalN":0.33,
        "Chg":17.54
    }

    OUTPUT DATA:
    {
        "ShouldTrade": true,
        "Symbol": "ADXS",
        "Quantity": 10000,
        "LimitPrice": `SignalP * 1.05`,
        "ProfitPrice": `SignalP * (1 + .05 + .08)`,
        "StopLoss": `SignalP * (1 - .05)`
    }

    OUTPUT ON ERROR:
    {
        ShouldTrade" false
    }
'''
# calculate_trading_params is super dumb at the moment
# the nice thing is the config.json is all that has to be updated
# and logic can be added to this one function without needing to 
# understand the rest of the system.
def calculate_trading_params(data):
    enter_over = config.get_config_value("trading_params.enter_at_percentage_over") + 1
    take_profit = config.get_config_value("trading_params.take_profit_percentage") + enter_over
    cut_loss = 1 - config.get_config_value("trading_params.cut_loss_percentage")

    results = {'ShouldTrade':False}

    # Here we just don't waste compute power on premarket data we can also add other validation
    if "SignalP" not in data:
        return results

    try:
        # Check Meets resistance Critira
        if data['Resistance'] >= config.get_config_value("trading_params.min_reistance_to_enter"):
            results['ShouldTrade'] = True
        else:
            return results

        # Add our existing params
        results["Symbol"] = data["Symbol"]
        results["Quantity"] = config.get_config_value("trading_params.num_of_shares")
        results["LimitPrice"] = data['SignalP'] * enter_over
        results["ProfitPrice"] = data['SignalP'] * take_profit
        results["StopLoss"] = data['SignalP'] * cut_loss
    except Exception as e:
        config.log_msg('Failed to compute trade on: {0}'.format(json.dumps(data)), name="trade", level="err")
        print(e)
        return {'ShouldTrade':False}

    return results