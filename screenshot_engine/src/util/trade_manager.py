from ib.opt import Connection, message
from ib.ext.Contract import Contract
from ib.ext.Order import Order

from copy import copy

from src.util import config
from src.util import rand_utils

class TradeManager(object):
    tm = None

    # The logic here is pretty straight forward. Eventually I want to play around with this
    # Inside threads. so by calling this method if a connection doesn't exist in that thread
    # we create a new one.
    @staticmethod
    def get_trade_manager():
        if TradeManager.tm is None:
            tm = TradeManager()
        return tm

    # get_next_bracket_order_id copies the current order id. It adds 3 for the next order.
    # finally it returns the coppied value. The reason we add three is a bracket order consists of
    # 3 orders.
    @DeprecationWarning
    def get_next_bracket_order_id(self):
        rsp = copy(self.current_order_id)
        self.current_order_id += 3
        return rsp

    def __init__(self, cid=rand_utils.rand_int()):
        # Read Config Params
        tws_host:str = config.get_config_value("ib_connection.host")
        tws_port:int = config.get_config_value("ib_connection.port")

        # Init IB stuff
        self.current_order_id = 0
        self.current_holdings = {}

        try:
            self.conn = Connection.create(host=tws_host, port=tws_port, clientId=cid)
            self.conn.connect()
        except Exception as e:
            print(e)
            config.log_msg("Failed to connect to TWS API. Will not be able to process trades!", "TRADE", "err", True)

    def is_holding(self, symbol):
        return symbol.lower() in self.current_holdings

    def add_holding(self, symbol, parent_order_id):
        self.current_holdings[symbol.lower()] = parent_order_id

    # NOTE This is not thread safe! Don't Try
    # Called by exec Orders
    def next_order_id(self):
        self.current_order_id += 1
        return self.current_order_id

    # TODO: error handling
    def filter_existing_positions(self, data_list):
        return [row for row in data_list if not self.is_holding(row['Symbol'])]

    # Called from run.py
    '''
    INPUT DATA
        [{
            "ShouldTrade": true,
            "Symbol": "ADXS",
            "Quantity": 10000,
            "LimitPrice": 1.111,
            "ProfitPrice": 1.1111,
            "StopLoss": 1.1111
        },...]
    '''
    # TODO:!!!! ADD ERROR LOGGING AND HANDLING !!!!
    # TODO: Check conn is still open. the TCP stack can be inconsistent.
    def exec_orders(self, data_list):
        # Remove Orders that are false
        filtered = [t for t in data_list if t['ShouldTrade'] == True]

        # For all the orders create the bracket and contract
        for order in filtered:
            # Get Params
            parent_order_id = self.next_order_id()
            symbol = order['Symbol']
            quantity = float(order['Quantity'])
            limit_price = float(order['LimitPrice'])
            profit_price = float(order['ProfitPrice'])
            stop_loss = float(order['StopLoss'])

            # Set current Possiton. we also store the order id for future use cases
            self.add_holding(symbol, parent_order_id)

            # Create our Contract
            contract = self.make_contract(symbol)

            # Create our list of orders for the bracket order.
            # b_orders = [parent, take_profit, cut_loss]
            b_orders = self.make_bracket_orders(parent_order_id, quantity, limit_price, profit_price, stop_loss)

            # Place the orders This has to be done sync. acync fucks shit up
            for o in b_orders:
                self.conn.placeOrder(o.orderId, contract, o)
                # We call next_order_id() to increment because a b order is actually 3 independant orders
                self.next_order_id()

    # Called from Exec Orders
    def make_bracket_orders(self, order_id:int, quantity:float, limit_price:float, take_profit_limit_price:float, stop_loss_price:float, action="BUY"):
        # Our buy order
        parent = Order()
        parent.orderId = order_id
        parent.action = action
        parent.orderType = "LMT"
        parent.totalQuantity = quantity
        parent.lmtPrice = limit_price
        parent.transmit = False

        # Our profit order
        take_profit = Order()
        take_profit.orderId = parent.orderId + 1
        take_profit.action = "SELL" if action == "BUY" else "BUY"
        take_profit.orderType = "LMT"
        take_profit.totalQuantity = quantity
        take_profit.lmtPrice = take_profit_limit_price
        take_profit.parentId = order_id
        take_profit.transmit = False

        # Our cut loss order
        stop_loss = Order()
        stop_loss.orderId = parent.orderId + 2
        stop_loss.action = "SELL" if action == "BUY" else "BUY"
        stop_loss.orderType = "STP"
        stop_loss.auxPrice = stop_loss_price
        stop_loss.totalQuantity = quantity
        stop_loss.parentId = order_id
        stop_loss.transmit = True

        # Return a list of orders to run. These orders have to be ran in that order
        # or shit hits the fan. 
        return [parent, take_profit, stop_loss]

    # Called from Exec Orders
    def make_contract(self, symbol, sec_type="STK", exch="SMART", prim_exchange="SMART", curr="USD"):
        Contract.m_symbol = symbol
        Contract.m_secType = sec_type
        Contract.m_exchange = exch
        Contract.m_primaryExch = prim_exchange
        Contract.m_currency = curr
        return Contract
