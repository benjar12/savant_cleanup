import time
import uuid
import json

from datetime import datetime

from src.util import config
from src.util import async_exec
from src.util import execute_screenshot
from src.util import s3_util
from src.util import trade_manager
from src.util import text_extract_util
from src.util import trading_requirements

def start_capture(start_delay, duration, pause):
    config.log_msg("Written by Ben Jarman<benjarman@me.com>", level="info", cant_recover=False)
    config.log_msg("Please contact Ben with questions or issues.", level="info", cant_recover=False)

    # This might not be the ideal home for a litteral, but it's needed none the less
    todays_dir = datetime.now().strftime('%Y/%m/%d')

    # Just a message to let were have started and make sure params are correct
    config.log_msg("Process is going to start capturing in {0} Seconds. Get STT open and sized.".format(start_delay))
    config.log_msg("Caputure will run for {0} Minutes".format(duration))
    config.log_msg("Program Will wait {0} Seconds inbetween each capture".format(pause))

    # Wait for all the appllications to get in position. stocks to trade is the big one.
    time.sleep(start_delay)

    # Lets get the async manager in place
    # TODO: REMEMBER at the end of the loop to flush the que
    ac = async_exec.AsyncTaskManager()
    tm = trade_manager.TradeManager()

    # One seconds has passed the duration we will go crall back into our hole untill tommorrow.
    timeout = time.time() + 60 * duration

    # Start the loop... There is a much cleaner way for sure
    while True:
        # we sleep here in case any part of the loop continues
        time.sleep(pause)

        # Are we out of time for today?
        if time.time() > timeout:
            break
        
        img_id = str(uuid.uuid1())
        object_key = "{0}/{1}".format(todays_dir, img_id)

        # Lets get those png bytes
        screenshot_bytes = execute_screenshot.capture_screen()

        # We can go a head and write this to S3. Extention wefaults to png but can be overwritten
        ac.s3_image_writter_pool(s3_util.write_bytes_to_s3, body=screenshot_bytes, filename=object_key)

        # Now we can just go a head and extract the table data
        try:
            list_of_items = text_extract_util.run_all_proccessing(object_key, screenshot_bytes)
        except Exception as e:
            config.log_msg("failed to extract table from {0}.png".format(object_key))
            print(e)
            continue

        # Next lets filter active positions for today
        # If filter Fails we should cut our loses because it means the entire screenshot 
        # failed.
        # TODO: Don't fail the entire screenshot, just the items that don't contain Symbol
        try:
            filtered = tm.filter_existing_positions(list_of_items)
            config.log_msg("Filtering passed")
        except:
            config.log_msg("Filtering failed on: {0}. Going to skip".format(object_key), level="warn")
            print(json.dumps(list_of_items[0]))
            continue

        # Now we can async calcuate the params to send to trade.
        config.log_msg("Computing should trade and trade params")
        ready_to_be_traded = ac.run_function_with_list_of_data_async(Method=trading_requirements.calculate_trading_params, DataList=filtered)

        # exec
        config.log_msg("Sending trading batch to execute orders")
        tm.exec_orders(ready_to_be_traded)



# Redundant but needed for being able to interprate and compile to C
def __main__():
    config.log_msg("Screen shot engine is ready to go")
    # Get Config and lets get this bitch on the road
    wait_to_start = config.get_config_value("init.startup_delay")
    minutes_to_run = config.get_config_value("init.run_for_n_minutes")
    wait_inbetween_shots = config.get_config_value("init.delay_between_captures")
    
    start_capture(wait_to_start, minutes_to_run, wait_inbetween_shots)



if __name__ == "__main__":
    __main__()
