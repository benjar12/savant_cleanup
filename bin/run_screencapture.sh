# readlink /proc/$$/exe

# This is a little redundant, but I want these scripts to be idiot proof
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../sbin";

# Lets get all our enviorment vars set
source $SCRIPT_DIR/set_paths.sh;

# TODO: Add Better Validation that params are passed in
VENV_POSTFIX=$1
if [ "$VENV_POSTFIX" == "" ]; then
    echo -e "${SA_RED}No venv postfix past in. Following convention it would be one of window|osx|linux${SA_NC}";
    echo -e "${SA_BLUE}Make sure you have created the venv${SA_NC}";
    echo -e "${SA_BLUE}CMD should look like this ./bin/run_screencapture.sh osx${SA_NC}";
    echo -e "${SA_RED}Application Terminating${SA_NC}";
    exit 1;
fi

# Deactivate any existing sessions. there should not be so we will just pass all output to /dev/null
deactivate > /dev/null 2>&1;

# Activate the env
# TODO: get status code and handle errors
SC_VENV_DIR="$APP_ROOT/venv_$VENV_POSTFIX";
echo -e "${SA_BLUE}ATTEMPTING TO ACTIVATE: $SC_VENV_DIR ${SA_NC}";
eval "source $SC_VENV_DIR/bin/activate";

# Add the source to python path
SCREEN_SHOT_CODE_DIR="$APP_ROOT/screenshot_engine";
export OLD_PYTHONPATH=$PYTHONPATH
export PYTHONPATH=$SCREEN_SHOT_CODE_DIR:$PYTHONPATH;
echo -e "${SA_BLUE}$SCREEN_SHOT_CODE_DIR Has been added to the path${SA_NC}";

# TODO: get status code handle non 0 exit codes;
echo -e "${SA_BLUE}Starting the program${SA_NC}";
eval "$SC_VENV_DIR/bin/python $SCREEN_SHOT_CODE_DIR/src/run.py";

echo -e "${SA_BLUE}Application has finished for today. Cleanning up${SA_NC}";
export PYTHONPATH=$OLD_PYTHONPATH
deactivate;