# readlink /proc/$$/exe

## env vars required by exe's
export WIN_DEV_ROOT="c://Development";
export WIN_APP_ROOT=$WIN_DEV_ROOT/savant_cleanup;

## bash env vars
export DEV_ROOT="/mnt/c/Development";
export APP_ROOT=$DEV_ROOT/savant_cleanup;

## Activate venv
source $DEV_ROOT/venv_windows/Scripts/activate;

## Update path
export PATH=/mnt/c/venv_windows/Scripts:$PATH

## Remove last build
rm -rf $APP_ROOT/build_windows;

## Build
pyinstaller.exe --onefile screenshot_engine/src/run.py -p screenshot_engine/ --distpath $WIN_APP_ROOT/build_windows -n start_trading -c;

## Copy config
cp $APP_ROOT/etc/screenshot_engine.json $APP_ROOT/build_windows/trading_config.json
