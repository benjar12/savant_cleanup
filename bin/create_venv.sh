# readlink /proc/$$/exe

SCRIPT_HELP='This script should be ran as such:\n`./bin/chreate_venv.sh REQUIRED:[osx|linux] OPTIONAL:[PATH TO PYTHON3 EXE]`'

# This is a little redundant, but I want these scripts to be idiot proof
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../sbin";

# Lets get all our enviorment vars set
source $SCRIPT_DIR/set_paths.sh;

# Provide a bit of help for frash osx machines.
echo -e "\n${SA_YELLOW}NOTICE: this script depends on python3.* 3.7 is recommened. \nThis script also requires pip3.7 and virtualenv.${SA_NC}";
echo -e "If you are running into a lot of issues and on osx run the following commands.";
echo -e "${SA_BLUE}-> brew install sashkab/python/python@3.7 ${SA_NC}";
echo -e "${SA_BLUE}-> /usr/local/bin/pip3.7 install --upgrade pip ${SA_NC}";
echo -e "${SA_BLUE}-> sudo /usr/local/bin/pip3.7 install virtualenv ${SA_NC}";
sleep 3;

# Setup our args that are going to be used throughout the script
SELECTED_OS=$1;
OPT_PYTHON_INTURPRITOR=$2;
SHOULD_EXIT_STRING="";
PYTHON_EXE="";
VENV_PATH=$APP_ROOT;

# TODO: VALIDATE virtualenv cmd is on the path HERE!

# VALIDATE OSX, which is reall just a postfix to to the folder name
if [ "$SELECTED_OS" == "" ]; then
    echo -e "${SA_RED}}No OS selected for venv accepted are linux or osx${SA_NC}
${SA_BLUE} $SCRIPT_HELP ${SA_NC}
${SA_RED}failed aborting application${SA_NC}";
    exit 1;
fi

# Lets make sure the directory does not already exist
VENV_DIR_NAME="venv_"$SELECTED_OS;
VENV_PATH=$VENV_PATH$VENV_DIR_NAME;
if [ -d "$VENV_PATH" ]; then
    echo -e "${SA_YELLOW}Folder venv_$SELECTED_OS Already exists at the project root.
delete this folder before running again.${SA_NC}
${SA_RED}failed aborting application${SA_NC}"
    exit 1;
fi

# Check if we use the system python or take in the argumant.
# This is useful because we might want to change versions for 
# different applications.
if [ "$OPT_PYTHON_INTURPRITOR" == "" ]; then 
    echo -e "${SA_BLUE}No python executable passed in. Going to attempt to use python on the PATH${SA_NC}";
    PYTHON_EXE=`which python`;
elif [ "$OPT_PYTHON_INTURPRITOR" != "" ]; then
    echo -e "${SA_BLUE}Using passed in param: $OPT_PYTHON_INTURPRITOR${SA_NC}";
    export PYTHON_EXE=$OPT_PYTHON_INTURPRITOR;
fi

# Now we need to check the python version is equal to 3.*
PYTHON_VERSION=`$PYTHON_EXE -c 'import platform; print(platform.python_version())'`;
if [[ ! "$PYTHON_VERSION" =~ "3." ]]; then
    echo -e "${SA_RED}Python did not match any supported versions${SA_NC} results of validation command";
    echo -e "${SA_YELLOW}$PYTHON_VERSION${SA_NC}";
    echo -e "${SA_RED}Please ethier add python 3.7.* to the PATH var or pass it in as second argumant\nfailure abourting application.${SA_NC}";
    exit 1;
fi

echo -e "${SA_GREEN}Python Version: $PYTHON_VERSION is compatable; ${SA_NC}";

echo -e "${SA_GREEN}ALL VALIDATION PASSED!${SA_NC}${SA_BLUE}\nCreating virtual enviorment${SA_NC}";

# next up we create the virtual env
# TODO: Validate the end of the string output contains done or error
cd $APP_ROOT;
virtualenv -p $PYTHON_EXE $VENV_DIR_NAME

VENV_ACTIVATE_CMD="source $VENV_PATH/bin/activate";
echo -e "${SA_BLUE}Virtual Env successfully created\nTo Manually Active in the future use\n$VENV_ACTIVATE_CMD\nTo Deactivate simply run deactivate${SA_NC}"

$(`$VENV_ACTIVATE_CMD`)

# Im using absolute path here because my enviorment is fucked.
PIP_CMD="$VENV_PATH/bin/pip"

echo "Currently using $VENV_PATH/bin/python as our inturpritor";
echo "Currently using $VENV_PATH/bin/pip as our package manager";

echo -e "${SA_BLUE} Installing required packages. If you see any failures it may be related to cbinding. ${SA_NC}";
echo -e "${SA_YELLOW} WARNING: Error handling is not yet in place here ${SA_NC}"

# There is a cleaner way to do this.
# eval is resorting to violince.
# TODO: Fix ^
eval "$PIP_CMD install --upgrade pip"
eval "$PIP_CMD install Pillow"
eval "$PIP_CMD install boto3"
eval "$PIP_CMD install pyscreenshot"
eval "$PIP_CMD install dotty-dict"
eval "$PIP_CMD install jupyter"
eval "$PIP_CMD install gevent"
eval "$PIP_CMD install mss"
eval "$PID_CMD install https://github.com/pyinstaller/pyinstaller/archive/develop.tar.gz"
sleep 2
eval "$PID_CMD install pylint"
eval "$PID_CMD install ib-api"

echo -e "${SA_GREEN}Your Enviorment is all set. To manually activate use:${SA_NC}";
echo -e "${SA_BLUE}$VENV_ACTIVATE_CMD\nSwitching back to the OS provided python is as simple as running: \ndeactivate${SA_NC}";
echo -e "${SA_GREEN}HAPPY TRADING${SA_NC}";