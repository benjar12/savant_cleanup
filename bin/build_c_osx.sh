# readlink /proc/$$/exe

# This is a little redundant, but I want these scripts to be idiot proof.
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../sbin";

# Lets get all our enviorment vars set.
source $SCRIPT_DIR/set_paths.sh;

# Just making sure were in the project root.
cd $APP_ROOT;

# Clean old build
rm -rf $APP_ROOT/build_osx

# Lazy mode TODO: fix

# Activate the python enviorment
source ./venv_bundle/bin/activate;

# Update PYTHONPATH
export PROJECTPATH=$SCRIPT_DIR/../screenshot_engine;
export PYTHONPATH=$PROJECTPATH:$PYTHONPATH;

pyinstaller --onefile screenshot_engine/src/run.py -p screenshot_engine/ --distpath $APP_ROOT/build_osx -n start_trading;

cp $APP_ROOT/etc/screenshot_engine.json $APP_ROOT/build_osx/trading_config.json;